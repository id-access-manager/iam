module gitlab.com/id-access-manager/iam

go 1.21

require (
	github.com/fatih/color v1.16.0
	github.com/go-playground/assert/v2 v2.2.0
	gorm.io/gorm v1.25.7
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/sys v0.18.0 // indirect
)
