package pagination

import (
	"math"

	"gorm.io/gorm"
)

func Paginate(value interface{}, pagination *Pagination, db *gorm.DB) func(db *gorm.DB) *gorm.DB {
	var (
		totalRows int64
		rowsCount int64
	)
	db.Model(value).Count(&totalRows)
	db.Model(value).Offset(pagination.GetOffset()).Limit(pagination.GetLimit()).Count(&rowsCount)

	pagination.ObjectsTotal = totalRows
	pagination.ObjectsCount = rowsCount
	pagination.PagesCount = int(math.Ceil(float64(totalRows) / float64(pagination.PageSize)))

	return func(db *gorm.DB) *gorm.DB {
		return db.Offset(pagination.GetOffset()).Limit(pagination.GetLimit()).Order(pagination.GetSort())
	}
}
