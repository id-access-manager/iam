package pagination

import (
	"github.com/go-playground/assert/v2"
	"testing"
)

const (
	defaultPageSize int    = 10
	defaultPage     int    = 1
	defaultSort     string = "Id desc"
)

func TestNewPagination(t *testing.T) {
	var (
		pageSize = 11
		page     = 1
		sort     = "Id desc"
	)
	pagination := NewPagination(pageSize, page, sort)

	assert.Equal(t, pageSize, pagination.PageSize)
	assert.Equal(t, page, pagination.Page)
	assert.Equal(t, sort, pagination.Sort)
}

func TestPagination_GetLimit(t *testing.T) {
	var pageSize = 11

	pagination := NewPagination(0, 1, "Id asc")

	t.Log("Test Pagination GetLimit with none value generation")
	assert.Equal(t, defaultPageSize, pagination.GetLimit())
	t.Log("Test set value for none field")
	assert.Equal(t, defaultPageSize, pagination.PageSize)

	t.Log("Set not none value for PageSize in pagination")
	pagination.PageSize = pageSize

	t.Log("Test Pagination GetLimit with not none value generation")
	assert.Equal(t, pageSize, pagination.GetLimit())
}

func TestPagination_GetPage(t *testing.T) {
	var page = 2

	pagination := NewPagination(12, 0, "Id asc")

	t.Log("Test Pagination GetPage with none value generation")
	assert.Equal(t, defaultPage, pagination.GetPage())
	t.Log("Test set value for none field")
	assert.Equal(t, defaultPage, pagination.Page)

	t.Log("Set not none value for Page in pagination")
	pagination.Page = page

	t.Log("Test Pagination GetPage with not none value generation")
	assert.Equal(t, page, pagination.GetPage())
}

func TestPagination_GetSort(t *testing.T) {
	var sort = "Id asc"

	pagination := NewPagination(12, 1, "")

	t.Log("Test Pagination GetSort with none value generation")
	assert.Equal(t, defaultSort, pagination.GetSort())
	t.Log("Test set value for none field")
	assert.Equal(t, defaultSort, pagination.Sort)

	t.Log("Set not none value for Sort in pagination")
	pagination.Sort = sort

	t.Log("Test Pagination GetSort with not none value generation")
	assert.Equal(t, sort, pagination.GetSort())
}

func TestPagination_GetOffset(t *testing.T) {
	var (
		pageSize = 12
		page     = 3
	)

	calculateOffset := func(pageSize int, page int) int { return (page - 1) * pageSize }

	pagination := NewPagination(0, 0, "Id asc")

	t.Log("Test Pagination GetOffset with none value generation")
	assert.Equal(t, calculateOffset(defaultPageSize, defaultPage), pagination.GetOffset())

	t.Log("Set not none value for PageSize and Page in pagination")
	pagination.PageSize = pageSize
	pagination.Page = page

	t.Log("Test Pagination GetOffset with not none value generation")
	assert.Equal(t, calculateOffset(pageSize, page), pagination.GetOffset())
}
