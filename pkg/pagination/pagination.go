package pagination

type Pagination struct {
	PageSize     int    `json:"page_size,omitempty;query:limit"`
	Page         int    `json:"page,omitempty;query:page"`
	Sort         string `json:"sort,omitempty;query:sort"`
	ObjectsTotal int64  `json:"objects_total"`
	ObjectsCount int64  `json:"objects_count"`
	PagesCount   int    `json:"pages_count"`
	Items        []any  `json:"items"`
}

func NewPagination(pageSize int, page int, sort string) *Pagination {
	return &Pagination{PageSize: pageSize, Page: page, Sort: sort}
}

func (p *Pagination) GetOffset() int {
	return (p.GetPage() - 1) * p.GetLimit()
}

func (p *Pagination) GetLimit() int {
	if p.PageSize == 0 {
		p.PageSize = 10
	}
	return p.PageSize
}

func (p *Pagination) GetPage() int {
	if p.Page == 0 {
		p.Page = 1
	}
	return p.Page
}

func (p *Pagination) GetSort() string {
	if p.Sort == "" {
		p.Sort = "Id desc"
	}
	return p.Sort
}
